# Table Of Content
1. [Required Software Installation](#required_software_installation)
2. [Installing ESP32 Development Environment](#esp32_development_env_installation)
3. [Compilation and Flashing](#compile_and_flash)
4. [Other](#other)

## 1. Required Software Installation<a name="required_software_installation"></a>
We need to install following Softwares for development.

### 1.1 Python:
- Download latest `python v2.7.x` from [here][python_download_link].
- Run installer.
- Select `Install for all users`. `Next`.
-  Leave installation path as default `C:\Python27`. `Next`.
- `Next`
- `Finish`
- For adding python to `PATH`, Open `Computer Properties`. Go to `Advanced System Settings`, Go to `Advanced` tab. Click on `Environment Variables..` button.
- Double click on `PATH` or `Path` in `System variables`.
- **Windows 7** :  Append `;C:\Python27` at the end.
- **Windows 10** : Click on `New` and add `C:\Python27`
- Click on `OK`. >> `OK`>> `OK`.

### 1.2 git:
- Download latest `git-scm` installer for your system from [here][git_download_link].
- Run Installer.
- `Next`.
- `Next`.
- Select `Use Git from the windows Command Prompt`. `Next`.
- Select `Use OpenSSH`. `Next`. Select `Use the OpenSSL Library`. `Next`.
- Select `Checkout Windows-stype, commit Unix-style line endings`. `Next`.
- Select `Use MinTTY(...)`. `Next`.
- Select `Enable File System caching` and `Enable Git Credential Manager`. `Install`.
- `finish`

### 1.3 Eclipse IDE:
- Download latest `Eclipse IDE Neon for C/C++ Developers` from [here][eclipse_download_link].
- Open downloaded zip file and extract it to `C:\` drive.
- Right Click on `C:\eclipse\eclipse.exe` and **Send to** `Desktop (create shortcut)`.
- Open `eclipse` from desktop shortcut. Set Workspace path and check `Use this as default and do not ask again` and press `OK`.

## 2. Installing ESP32 Development Environment <a name="esp32_development_env_installation"></a>
- Download latest `toolchain` from [here][esp32_toolchain_download_link].
- Open downloaded zip file and `extract to` `C:\` drive.
- Go to `C:\msys32\` and double click on `msys2_shell.bat`. You should get output as follow. If not please reboot your system and retry.

![ESP32 Install result][esp32_install_result]

- Enter `cd C:` command. Then Enter `mkdir esp32 esp32_proj && cd esp32`.
- Now enter `git clone --recursive https://github.com/espressif/esp-idf.git`.
- Now Enter `cd C:\esp32_proj` and enter `git clone https://sandeepsondagar@gitlab.com/sandeepsondagar/esp32.git`.
- Now enter `cd esp/gatt_server`.
- Now enter `export IDF_PATH="C:\esp32\esp-idf`.
- Now enter `make menuconfig`. This will open window as below.

![make menuconfig][make_menuconfig_image]

- Go to `Serial Flasher Config` >> `Default Serial port` and enter your esp32 serial port. (e.g. `COM4`)

![com port selection][com_select]

- `OK` >> `Exit`. >> `Exit`. Select `OK` or `Yes` if propted for `Save Configuration`.

## 3. Compile firmware and flash firmware. <a name="compile_and_flash"></a>
- Open `Eclipse IDE`.
- `File` >> `Open Projects from File System...` >> Select Directory `C:\esp32_proj\esp32\gatt_server`. Make sure you have selected `gatt_server` project. >> `Finish`.

![Open Project][open_esp32_project]

- Now Expand `gatt_server` project under `Project Explorer` window.
- To build project, `Project` menu >> `Build Project`.

![Build Project][build_project]

- To Flash project, under `Project Explorer` >> `gatt_server` >> `Build Targets` >> Double Click on `flash`.

![flash esp32 firmware][flash_esp32_firmware]

- You can also flash firmware from `Build Targets` window on right side >> `gatt_project` >> `flash`.

![flash esp32 firmware][flash_esp32_firmware2]

- To clean project, `Project` menu >> `Clean...`.

## 4. Other<a name="other"></a>
- I can help you to setup development environment and IDE via TeamViewer. Please let me know.
- If you face any issue in any step, then please message me, I will reply ASAP.

[python_download_link]: https://www.python.org/downloads/  "Download Python 2.7.x"
[git_download_link]: https://git-scm.com/download/win "Download git-scm for windows"
[eclipse_download_link]: http://www.eclipse.org/downloads/packages/eclipse-ide-cc-developers/neon3 "Download Eclipse Neon for C/C++ Developers"
[esp32_toolchain_download_link]: https://dl.espressif.com/dl/esp32_win32_msys2_environment_and_toolchain-20170330.zip "Download ESP32 toolchain"
[esp32_install_result]: http://res.cloudinary.com/raspi-iot/image/upload/v1493180633/image1_o9aeim.png "ESP32 Install Result"
[make_menuconfig_image]: http://res.cloudinary.com/raspi-iot/image/upload/v1493182085/image2_wc355g.png "make menuconfig."
[open_esp32_project]: http://res.cloudinary.com/raspi-iot/image/upload/v1493182754/image3_evjckz.png "Open ESP32 Project"
[flash_esp32_firmware]: http://res.cloudinary.com/raspi-iot/image/upload/v1493183206/image4_risgor.png "Flash ESP32 Firmware"
[flash_esp32_firmware2]: http://res.cloudinary.com/raspi-iot/image/upload/v1493183484/image5_ojkgm5.png "Flash ESP32 Firmware"
[com_select]: http://res.cloudinary.com/raspi-iot/image/upload/v1493183762/image6_p9kc7o.png "Select COM port"
[build_project]:http://res.cloudinary.com/raspi-iot/image/upload/v1493184309/image7_cafina.png "Build Project"